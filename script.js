$(document).ready(function() {
    "use strict";
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
    var gCoursesDB = {
        description: "This DB includes all courses in system",
        courses: [
            {
                id: 1,
                courseCode: "FE_WEB_ANGULAR_101",
                courseName: "How to easily create a website with Angular",
                price: 750,
                discountPrice: 600,
                duration: "3h 56m",
                level: "Beginner",
                coverImage: "images/courses/course-angular.jpg",
                teacherName: "Morris Mccoy",
                teacherPhoto: "images/teacher/morris_mccoy.jpg",
                isPopular: false,
                isTrending: true
            },
            {
                id: 2,
                courseCode: "BE_WEB_PYTHON_301",
                courseName: "The Python Course: build web application",
                price: 1050,
                discountPrice: 900,
                duration: "4h 30m",
                level: "Advanced",
                coverImage: "images/courses/course-python.jpg",
                teacherName: "Claire Robertson",
                teacherPhoto: "images/teacher/claire_robertson.jpg",
                isPopular: false,
                isTrending: true
            },
            {
                id: 5,
                courseCode: "FE_WEB_GRAPHQL_104",
                courseName: "GraphQL: introduction to graphQL for beginners",
                price: 850,
                discountPrice: 650,
                duration: "2h 15m",
                level: "Intermediate",
                coverImage: "images/courses/course-graphql.jpg",
                teacherName: "Ted Hawkins",
                teacherPhoto: "images/teacher/ted_hawkins.jpg",
                isPopular: true,
                isTrending: false
            },
            {
                id: 6,
                courseCode: "FE_WEB_JS_210",
                courseName: "Getting Started with JavaScript",
                price: 550,
                discountPrice: 300,
                duration: "3h 34m",
                level: "Beginner",
                coverImage: "images/courses/course-javascript.jpg",
                teacherName: "Ted Hawkins",
                teacherPhoto: "images/teacher/ted_hawkins.jpg",
                isPopular: true,
                isTrending: true
            },
            {
                id: 8,
                courseCode: "FE_WEB_CSS_111",
                courseName: "CSS: ultimate CSS course from beginner to advanced",
                price: 750,
                discountPrice: 600,
                duration: "3h 56m",
                level: "Beginner",
                coverImage: "images/courses/course-javascript.jpg",
                teacherName: "Juanita Bell",
                teacherPhoto: "images/teacher/juanita_bell.jpg",
                isPopular: true,
                isTrending: true
            },
            {
                id: 14,
                courseCode: "FE_WEB_WORDPRESS_111",
                courseName: "Complete Wordpress themes & plugins",
                price: 1050,
                discountPrice: 900,
                duration: "4h 30m",
                level: "Advanced",
                coverImage: "images/courses/course-wordpress.jpg",
                teacherName: "Clevaio Simon",
                teacherPhoto: "images/teacher/clevaio_simon.jpg",
                isPopular: true,
                isTrending: false
            }
        ]
    }
    var gDataElement = {};
    var vColCourse = ["id","courseCode","courseName","price","discountPrice","duration","level","teacherName","isPopular","isTrending","action"];
    const vID_COL = 0;
    const vCOURSE_CODE_COL = 1;
    const vCOURSE_NAME_COL = 2;
    const vPRICE_COL = 3;
    const vDISCOUNT_PRICE_COL = 4;
    const vDURATION_COL = 5;
    const vLEVEL_COL = 6;
    const vTEACHER_NAME_COL = 7;
    const vIS_POPULAR_COL = 8 ;
    const vIS_TRENDING_COL = 9;
    const vACTION_COL = 10;
    /*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
    onPageloading();
    // BTN CREATE
    $("#btn-create-course").on("click",onBtnCreateCourse);
    $("#btn-create").on("click",onBtnCreate);
    // BTN FIX
    $(document).on("click","#table-course .btn-fix",onBtnFixClick);
    $("#btn-edit").on("click",onBtnEditClick);
    // BTN DELETE
    $(document).on("click","#table-course .btn-delete",onBtnDeleteClick);
    $("#btn-confirm").on("click",onBtnConfirmClick);

    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    function onPageloading() {
        // Bước 1: thu thập dữ liệu
        var vCoursePopular = [];
        var vCourseTrending = [];
        getCoursePopular(vCoursePopular);
        getCourseTrending(vCourseTrending);
        // Bước 2: xử lý hiện thị
        showCoursePopular(vCoursePopular);
        showCourseTrending(vCourseTrending);
        loadDataToTable();
       
    }

    // CREATE COURSE
    function onBtnCreateCourse() {
        $("#create-course-modal").modal("show");
    }
    function onBtnCreate() {
        // Bước 1: thu thập dữ liệu
        var vDataFormInp = {};
        getDataFormInp(vDataFormInp);
        // Bước 2: validate
        var isValidate = checkDataFormInp(vDataFormInp);
        if(!isValidate) {
            return;
        }
        createCourse(vDataFormInp);
        loadDataInpToTable();
        resetDataInp();
        $("#create-course-modal").modal("hide");
    } 

    // FIX COURSE
    function onBtnFixClick() {
        getDataNeedFix(this);
        $("#fix-course-modal").modal("show");
    }
    function onBtnEditClick() {
        var vDataEdit= {};
        getDataEdit(vDataEdit);
        var isValidate = checkDataFixInp(vDataEdit);
        if(!isValidate) {
            return;
        }
        editData(vDataEdit);
        loadDataInpToTable();
        $("#fix-course-modal").modal("hide");
    }
    // DELETE COURSE
    function onBtnDeleteClick() {
        getDataNeedDelete(this);
        $("#delete-course-modal").modal("show");
    }
    function onBtnConfirmClick() {
        deleteData();
        loadDataInpToTable();
        $("#delete-course-modal").modal("hide");
    }

    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
    function getCoursePopular(paramCoursePopular) {
        for( var bI in gCoursesDB.courses) {
            if(gCoursesDB.courses[bI].isPopular == true) {
                 paramCoursePopular.push(gCoursesDB.courses[bI]);
            }
        }
    }
    function  getCourseTrending(paramCourseTrending) {
        for( var bI in gCoursesDB.courses) {
            if(gCoursesDB.courses[bI].isTrending == true) {
                 paramCourseTrending.push(gCoursesDB.courses[bI]);
            }
        }
    }

    function showCoursePopular(paramCoursePopular) {
        var vCardPopular = $("#card-course-popular");
        for(var bI in paramCoursePopular) {
            vCardPopular.append(`
            <div class="col-lg-3 col-md-6 mb-4 justify-content-center">
                <div class="card card-height mx-auto">
                  <img class="card-img-top" src=${paramCoursePopular[bI].coverImage}>
                  <div class="card-body">
                    <h6 class="card-title mb-2">
                      <a href="#">${paramCoursePopular[bI].courseName}</a>
                    </h6>
                    <ul class="list-inline mb-3">
                      <li class="list-inline-item">
                        <i class="far fa-clock mr-2"></i>${paramCoursePopular[bI].duration}
                      </li>
                      <li class="list-inline-item">${paramCoursePopular[bI].level}</li>
                    </ul>
                    <span class="text-dark font-weight-bold">$${paramCoursePopular[bI].price}</span>
                    <del class="text-muted">$${paramCoursePopular[bI].discountPrice}</del>
                  </div>
                  <div class="card-footer">
                    <div class="float-left mr-3">
                      <img src=${paramCoursePopular[bI].teacherPhoto} class="rounded-circle img-teacher-avatar">
                    </div>
                    <div class="float-left">
                      <small>${paramCoursePopular[bI].teacherName}</small>
                    </div>
                    <div class="float-right">
                      <i class="far fa-bookmark text-secondary"></i>
                    </div>
                </div>
            </div>`);
        }
    }
    function showCourseTrending(paramCourseTrending) {
        var vCardTrending = $("#card-course-trending");
        for(var bI in paramCourseTrending) {
            vCardTrending.append(`
            <div class="col-lg-3 col-md-6 mb-4 justify-content-center">
                <div class="card card-height mx-auto">
                  <img class="card-img-top" src=${paramCourseTrending[bI].coverImage}>
                  <div class="card-body">
                    <h6 class="card-title mb-2">
                      <a href="#">${paramCourseTrending[bI].courseName}</a>
                    </h6>
                    <ul class="list-inline mb-3">
                      <li class="list-inline-item">
                        <i class="far fa-clock mr-2"></i>${paramCourseTrending[bI].duration}
                      </li>
                      <li class="list-inline-item">${paramCourseTrending[bI].level}</li>
                    </ul>
                    <span class="text-dark font-weight-bold">$${paramCourseTrending[bI].price}</span>
                    <del class="text-muted">$${paramCourseTrending[bI].discountPrice}</del>
                  </div>
                  <div class="card-footer">
                    <div class="float-left mr-3">
                      <img src=${paramCourseTrending[bI].teacherPhoto} class="rounded-circle img-teacher-avatar">
                    </div>
                    <div class="float-left">
                      <small>${paramCourseTrending[bI].teacherName}</small>
                    </div>
                    <div class="float-right">
                      <i class="far fa-bookmark text-secondary"></i>
                    </div>
                </div>
            </div>`);
        }
    }
    function loadDataToTable() {
        $("#table-course").DataTable({
            searching:false,
            data: gCoursesDB.courses,
            columns: [
                { data: vColCourse[vID_COL] },
                { data: vColCourse[vCOURSE_CODE_COL] },
                { data: vColCourse[vCOURSE_NAME_COL] },
                { data: vColCourse[vPRICE_COL] },
                { data: vColCourse[vDISCOUNT_PRICE_COL] },
                { data: vColCourse[vDURATION_COL] },
                { data: vColCourse[vLEVEL_COL] },
                { data: vColCourse[vTEACHER_NAME_COL] },
                { data: vColCourse[vIS_POPULAR_COL] },
                { data: vColCourse[vIS_TRENDING_COL] },
                { data: vColCourse[vACTION_COL] }
              ],
              columnDefs: [
                { // định nghĩa lại cột STT
                  targets: vID_COL,
                  render:  function (data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                  }
                },
                { // định nghĩa lại cột action
                  targets: vACTION_COL,
                  defaultContent: `
                  <button class="btn-fix" data-toggle="tooltip" data-placement="top" title="Edit course"><i class="fa-solid fa-screwdriver-wrench text-primary"></i></button>
                  <button class="btn-delete" data-toggle="tooltip" data-placement="top" title="Delete course"><i class="fa-solid fa-trash-can text-danger"></i></button>
                  `
                }
              ]
        });
    }
    // -----------CREATE COURSE-----------------
    function getDataFormInp(paramDataFormInp) {
        paramDataFormInp.id =  $("#inp-id").val().trim();
        paramDataFormInp.courseCode =  $("#inp-coursecode").val().trim();
        paramDataFormInp.courseName =  $("#inp-coursename").val().trim();
        paramDataFormInp.price =  $("#inp-price").val().trim();
        paramDataFormInp.discountPrice =  $("#inp-disprice").val().trim();
        paramDataFormInp.duration =  $("#inp-duration").val().trim();
        paramDataFormInp.level =  $("#inp-level").val().trim();
        paramDataFormInp.coverImage =  $("#inp-coverimg").val().trim();
        paramDataFormInp.teacherName =  $("#inp-teachername").val().trim();
        paramDataFormInp.teacherPhoto =  $("#inp-teacherphoto").val();
        paramDataFormInp.isPopular =  $("#select-popular").is(":checked");
        paramDataFormInp.isTrending =  $("#select-trending").is(":checked");
}
function checkDataFormInp(paramDataFormInp) {
    if(paramDataFormInp.id === "") {
        alert("chưa nhập ID");
        return false;
    }
    if(isNaN(paramDataFormInp.id)) {
        alert("ID phải là số");
        return false;
    }
    for(var bI in gCoursesDB.courses) {
        if(paramDataFormInp.id == gCoursesDB.courses[bI].id) {
            alert("ID đã được sử dụng");
            return false;
        }
    }
    if(paramDataFormInp.courseCode === "") {
        alert("chưa nhập courseCode");
        return false;
    }
    for(var bI in gCoursesDB.courses) {
     if(paramDataFormInp.courseCode == gCoursesDB.courses[bI].courseCode) {
         alert("CourseCode đã được sử dụng");
         return false;
     }
 }
    if(paramDataFormInp.courseName === "") {
        alert("chưa nhập courseName");
        return false;
    }
    if(paramDataFormInp.price === "") {
        alert("chưa nhập price");
        return false;
    }
    if(isNaN(paramDataFormInp.price)) {
     alert("price phải là số");
     return false;
     }
    if(paramDataFormInp.price < 0) {
     alert("price phải lớn hơn 0");
     return false;
     }
    if(paramDataFormInp.duration === "") {
        alert("chưa nhập duration");
        return false;
    }
    if(paramDataFormInp.level === "") {
        alert("chưa nhập level");
        return false;
    }
    if(paramDataFormInp.coverImage === "") {
        alert("chưa nhập coverimg");
        return false;
    }
    if(paramDataFormInp.teacherName === "") {
        alert("chưa nhập teacherName");
        return false;
    }
    if(paramDataFormInp.teacherPhoto === "") {
        alert("chưa nhập teacherPhoto");
        return false;
    }
    return true;
}
   function createCourse (paramDataFormInp) {
    gCoursesDB.courses.push(paramDataFormInp);
   }
   function resetDataInp() {
       $("#inp-id").val("");
       $("#inp-coursecode").val("");
       $("#inp-coursename").val("");
       $("#inp-price").val("");
       $("#inp-disprice").val("");
       $("#inp-duration").val("");
       $("#inp-level").val("");
       $("#inp-coverimg").val("");
       $("#inp-teachername").val("");
       $("#inp-teacherphoto").val("");
       $("#select-popular").prop("checked",false);
       $("#select-trending").val("checked",false);
   }
    //-----------FIX COURSE--------------------
    function getDataNeedFix(paramBtn) {
        "use strict"
        var vRowClick = $(paramBtn).closest("tr");
        var vTable = $("#table-course").DataTable();
        var vDataRow = vTable.row(vRowClick).data();
        gDataElement = vDataRow;

         $("#inp-fix-id").val(gDataElement.id);
         $("#inp-fix-coursecode").val(gDataElement.courseCode);
         $("#inp-fix-coursename").val(gDataElement.courseName);
         $("#inp-fix-price").val(gDataElement.price);
         $("#inp-fix-disprice").val(gDataElement.discountPrice);
         $("#inp-fix-duration").val(gDataElement.duration);
         $("#inp-fix-level").val(gDataElement.level);
         $("#inp-fix-coverimg").val(gDataElement.coverImage);
         $("#inp-fix-teachername").val(gDataElement.teacherName);
         $("#inp-fix-teacherphoto").val(gDataElement.teacherPhoto);
         $("#select-fix-popular").prop("checked",gDataElement.isPopular);
         $("#select-fix-trending").prop("checked",gDataElement.isTrending);
    }    
    function getDataEdit(paramDataEdit) {
        paramDataEdit.id =  $("#inp-fix-id").val().trim();
        paramDataEdit.courseCode =  $("#inp-fix-coursecode").val().trim();
        paramDataEdit.courseName =  $("#inp-fix-coursename").val().trim();
        paramDataEdit.price =  $("#inp-fix-price").val().trim();
        paramDataEdit.discountPrice =  $("#inp-fix-disprice").val().trim();
        paramDataEdit.duration =  $("#inp-fix-duration").val().trim();
        paramDataEdit.level =  $("#inp-fix-level").val().trim();
        paramDataEdit.coverImage =  $("#inp-fix-coverimg").val().trim();
        paramDataEdit.teacherName =  $("#inp-fix-teachername").val().trim();
        paramDataEdit.teacherPhoto =  $("#inp-fix-teacherphoto").val();
        paramDataEdit.isPopular =  $("#select-fix-popular").is(":checked");
        paramDataEdit.isTrending =  $("#select-fix-trending").is(":checked");
    }
    function checkDataFixInp(paramDataEdit) {
        if(paramDataEdit.id === "") {
            alert("chưa nhập ID");
            return false;
        }
        if(isNaN(paramDataEdit.id)) {
            alert("ID phải là số");
            return false;
        }
        console.log(gDataElement);
        for(var bI in gCoursesDB.courses) {
            if(gDataElement.id != paramDataEdit.id && paramDataEdit.id == gCoursesDB.courses[bI].id) {
                alert("ID đã được dùng");
                return false;
            }
        }
        if(paramDataEdit.courseCode === "") {
            alert("chưa nhập courseCode");
            return false;
        }
        for(var bI in gCoursesDB.courses) {
            if(gDataElement.courseCode != paramDataEdit.courseCode && paramDataEdit.courseCode == gCoursesDB.courses[bI].courseCode) {
                alert("CourseCode đã được dùng");
                return false;
            }
        }
        if(paramDataEdit.courseName === "") {
            alert("chưa nhập courseName");
            return false;
        }
        if(paramDataEdit.price === "") {
            alert("chưa nhập price");
            return false;
        }
        if(isNaN(paramDataEdit.price)) {
         alert("price phải là số");
         return false;
         }
        if(paramDataEdit.price < 0) {
         alert("price phải lớn hơn 0");
         return false;
         }
        if(paramDataEdit.duration === "") {
            alert("chưa nhập duration");
            return false;
        }
        if(paramDataEdit.level === "") {
            alert("chưa nhập level");
            return false;
        }
        if(paramDataEdit.coverImage === "") {
            alert("chưa nhập coverimg");
            return false;
        }
        if(paramDataEdit.teacherName === "") {
            alert("chưa nhập teacherName");
            return false;
        }
        if(paramDataEdit.teacherPhoto === "") {
            alert("chưa nhập teacherPhoto");
            return false;
        }
        return true;
    }
    function editData(paramDataEdit) {
        for(var bI in gCoursesDB.courses) {
            if(paramDataEdit.id == gCoursesDB.courses[bI].id) {
                gCoursesDB.courses.splice(bI,1,paramDataEdit);
            }
        }
    }
    // ----------------------------------------------
    function loadDataInpToTable() {
        "use strict"
        var vTable = $("#table-course").DataTable();
        vTable.clear();
        vTable.rows.add(gCoursesDB.courses);
        vTable.draw();
    }
    // -----------DELETE COURSE--------------
    function getDataNeedDelete(paramBtn) {
        "use strict"
        var vRowClick = $(paramBtn).closest("tr");
        var vTable = $("#table-course").DataTable();
        var vDataRow = vTable.row(vRowClick).data();
        gDataElement = vDataRow;
    }
    function deleteData() {
        for(var bI in gCoursesDB.courses) {
            if(gDataElement.id == gCoursesDB.courses[bI].id) {
                gCoursesDB.courses.splice(bI,1);
            }
        }
    }
});
